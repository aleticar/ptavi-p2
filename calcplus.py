import sys
from calcoo import Calculadora
import calcoohija
from calcoohija import CalculadoraHija


# class CalculadoraPlus(CalculadoraHija):
# def leerfichero(self, fichero):
# self.fichero = open(fichero, 'r')


if __name__ == '__main__':

    leer = open(sys.argv[1])
    op1 = sys.argv[1]
    op2 = op1
    e = CalculadoraHija(op1, op2)
    fich = open('fichero.csv', 'r')
    list = fich.readlines()
    for linea in list:
        # LineNum es directamente el elemento de la lista, no la posicion.
        numero = linea.split(',')
        # Crea una lista con los elementos que van separados por comas.
        Operation = numero.pop(0)
        try:
            if Operation == 'suma':
                Result = 0
                for Op in numero:
                    Result = Result + e.plus(int(Op), 0)
                print(Result)
            elif Operation == 'resta':
                Result = int(numero.pop(0))
                Sum = 0
                for Op in numero:
                    # A-B-C-D = A - (B + C + D)
                    Sum = Sum + e.plus(int(Op), 0)
                Result = Result - Sum
                print(Result)
            elif Operation == 'por':
                Result = 1
                for Op in numero:
                    Result = Result*e.multiplica(int(Op), 1)
                print(Result)
            elif Operation == 'entre':
                Result = int(numero.pop(0))
                try:
                    for Op in numero:
                        Result = Result/e.divide(int(Op), 1)
                    print(Result)
                except ZeroDivisionError:
                    sys.exit('Division by zero is not allowed')
            else:
                sys.exit('Possible operations: /suma/resta/divide/multiplica/')
        except ValueError:
            sys.exit("Non numerical parameters")

    fich.close()
