# !/usr/bin/python3
# -*- coding: utf-8 -*-

import sys
import csv
from calcoohija import CalculadoraHija

# class Calcplusplus(CalculadoraPlus):
# def leer(self,fichero):
# self.fichero = open(fichero, 'r')
if __name__ == "__main__":
    op1 = sys.argv[1]
    op2 = op1
    e = CalculadoraHija(op1, op2)
    with open(sys.argv[1], newline='') as csvfile:
        reader = csv.reader(csvfile, delimiter=',')
        # leer = open(sys.argv[1])

    # fich = open('fichero.csv', 'r')
    # list = fich.readlines()
        for row in reader:

            # salta = row.split(',')

            Oprow = row.pop(0)
            try:
                if Oprow == 'suma':
                    Result = 0
                    for Op in row:
                        Result = Result + e.plus(int(Op), 0)
                    print(Result)
                elif Oprow == 'resta':
                    Result = int(row.pop(0))
                    Sum = 0
                    for Op in row:
                        # Primer numero mas segundo mas ....
                        Sum = Sum + e.plus(int(Op), 0)
                    Result = Result - Sum
                    print(Result)
                elif Oprow == 'por':
                    Result = 1
                    for Op in row:
                        Result = Result * e.multiplica(int(Op), 1)
                    print(Result)
                elif Oprow == 'entre':
                    Result = int(row.pop(0))
                    try:
                        for Op in row:
                            Result = Result / e.divide(int(Op), 1)
                        print(Result)
                    except ZeroDivisionError:
                        sys.exit('Division by zero is not allowed')
                else:
                    sys.exit('Puedes sumar restar multiplicar o dividir')
            except ValueError:
                sys.exit("Non numerical parameters")
