
# !/usr/bin/python3
# -*- coding: utf-8 -*-
import sys


class Calculadora ():

    def __init__(self, op1, op2):
        """ Function to substract the operands """
        self.op1 = op1
        self.op2 = op2

    def minus(self, op1, op2):
        return self.op1 - self.op2

    def plus(self, op1, op2):
        return self.op1 + self.op2


# op1 = int(sys.argv[1])
# op2 = int(sys.argv[3])
# c = Calculadora(op1, op2)
if __name__ == "__main__":
    try:
        op1 = int(sys.argv[1])
        op2 = int(sys.argv[3])
        c = Calculadora(op1, op2)
    except ValueError:
        sys.exit("No son números para poder operar")

    if sys.argv[2] == "suma":
        result = c.plus(op1, op2)

    elif sys.argv[2] == "resta":
        result = c.minus(op1, op2)

    else:
        sys.exit('Operación sólo puede ser sumar o restar.')
    print(result)
