

import sys
from calcoo import Calculadora


class CalculadoraHija(Calculadora):

    def divide(self, op1, op2):

        if op2 == 0:
            sys.exit("Division by zero not allowed")
        else:
            return op1 / op2

    def multiplica(self, op1, op2):
        return op1*op2


# op1 = int(sys.argv[1])
# op2 = int(sys.argv[3])
# op3 = int(sys.argv[2])
# c = Calculadora(op1, op2)

if __name__ == "__main__":

    # Calculadora = CalculadoraHija(op1,op2)
    # Calculadora.__init__(sys.argv[1],sys.argv[3])

    try:

        op1 = int(sys.argv[1])
        op2 = int(sys.argv[3])
        c = Calculadora(op1, op2)
        d = CalculadoraHija(op1, op2)
    except ValueError:
        sys.exit("No son números para poder operar")
    if sys.argv[2] == "suma":
        result = c.plus(op1, op2)

    elif sys.argv[2] == "resta":
        result = c.minus(op1, op2)

    elif sys.argv[2] == "entre":
        result = d.divide(op1, op2)

    elif sys.argv[2] == "por":
        result = d.multiplica(op1, op2)

    else:
        sys.exit('Operación sólo puede ser sumar,restar,multiplar o dividir.')
    print(result)
